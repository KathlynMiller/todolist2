package android.millerk.todolist2;

import android.app.FragmentTransaction;
        import android.support.v4.app.Fragment;
        import android.os.Bundle;
        import android.support.v4.app.ListFragment;
        import android.view.Menu;
        import android.view.MenuItem;

import java.util.ArrayList;


public class MainActivity extends SingleFragmentActivity implements ActivityCallBack{

    ArrayList<ToDoPost> ToDoPost = new ArrayList<ToDoPost>();

    @Override
    protected Fragment createFragment() {

        return new ListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}