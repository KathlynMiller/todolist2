package android.millerk.todolist2;

import android.support.v4.app.Fragment;

        import android.net.Uri;
        import android.support.annotation.LayoutRes;



public class ToDoActivity extends SingleFragmentActivity  {

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected Fragment createFragment() {

        return new ToDoListFragment();
    }

}

